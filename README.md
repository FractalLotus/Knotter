Kotter
=======================================

About
---------------------------------------
This program is a designer for Celtic knotworks, using the mathematical
correlation between knots and graphs.

The software is under development, some features may change without
notice and there may be some bugs.


Contacts
---------------------------------------
Mattia Basaglia <mattia.basaglia@gmail.com>
[Knotter Website](http://knotter.mattbas.org/)

If you have any suggestion, criticism, feature request or bug report please let me know.


License
---------------------------------------
GPLv3 or later, see COPYING.


Build instructions
---------------------------------------

Quick build

    mkdir build
    cmake ..
    make

Dependencies
---------------------------------------

* Qt Framework, should work with 4.7 or later
* CMake 3.5 or layer


Getting the latest sources
---------------------------------------

The git repository is located at git://gitlab.com/mattbas/Knotter.git
You can browse it online at https://gitlab.com/mattbas/Knotter


### Cloning from Git ###

This source tree uses git submodules, this means that the submodules need to
be initialized when the repo is cloned:

    # Clone repo
    git clone git://gitlab.com/mattbas/Knotter.git

    # Initialize submodules
    git submodule update --init

